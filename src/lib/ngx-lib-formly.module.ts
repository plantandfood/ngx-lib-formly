import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { NgxLibMaterialModule } from 'ngx-lib-material';
import { FormViewComponent } from './formly-form/form-view/form-view.component';
import { FormlyFormFlexComponent } from './formly-form/formly-form-flex.component';
import { AutocompleteTypeComponent } from './types/autocomplete.component';
import { ButtonTypeComponent } from './types/button-type.component';
import { DateTimePickerComponent } from './types/datetime-picker/datetime-picker.component';
import { FileUploadWrapperComponent } from './types/file-upload-wrapper.component';
import { FileUploadComponent } from './types/file-upload.component';
import { FileValueAccessorDirective } from './types/file-value-accessor';
import { HtmlTypeComponent } from './types/html-type.component';
import { InfoFieldComponent } from './types/info-field/info-field.component';
import { NgSelectTypeComponent } from './types/ng-select.component';
import { RepeatTypeComponent } from './types/repeat-section.type';
import { TagInputComponent } from './types/tag-input.component';
import * as fromValidators from './util/validators';


@NgModule({
  imports: [
    CommonModule,
    NgxLibMaterialModule,
    NgSelectModule,
    FormsModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    FormlyMaterialModule,
    FormlyModule.forRoot({
      types: [
        {
          name: 'infofield',
          component: InfoFieldComponent
        },
        {
          name: 'datetimepicker',
          component: DateTimePickerComponent,
          wrappers: ['form-field']
        },
        {
          name: 'autocomplete',
          component: AutocompleteTypeComponent,
          wrappers: ['form-field']
        },
        {
          name: 'file-upload',
          component: FileUploadComponent,
          wrappers: ['form-field']
        },
        {
          name: 'html',
          component: HtmlTypeComponent,
          wrappers: ['form-field']
        },
        { name: 'ng-select', component: NgSelectTypeComponent },
        {
          name: 'tag-input',
          component: TagInputComponent,
          wrappers: ['form-field']
        },
        { name: 'repeat', component: RepeatTypeComponent },
        {
          name: 'button',
          component: ButtonTypeComponent,
          defaultOptions: {
            templateOptions: {
              type: 'button'
            }
          }
        }
      ],
      validators: [
        { name: 'integer', validation: fromValidators.integerValidator },
        { name: 'decimal', validation: fromValidators.decimalValidator },
        {
          name: 'contact-number',
          validation: fromValidators.contactNumberValidator
        },
        { name: 'email', validation: fromValidators.emailValidator },
        { name: 'file-upload', validation: fromValidators.fileUploadValidator },
        { name: 'pin', validation: fromValidators.pinValidator },
        {
          name: 'safe-input',
          validation: fromValidators.specialCharactersValidator
        }
      ],
      validationMessages: [
        {
          name: 'file-upload',
          message: fromValidators.fileUploadValidatorMessage
        },
        { name: 'required', message: 'this field is required' },
        {
          name: 'maxlength',
          message: fromValidators.maxlengthValidationMessage
        },
        { name: 'integer', message: fromValidators.integerValidatorMessage },
        { name: 'decimal', message: fromValidators.decimalValidatorMessage },
        {
          name: 'contact-number',
          message: fromValidators.contactNumberValidatorMessage
        },
        { name: 'email', message: fromValidators.emailValidatorMessage },
        {
          name: 'not-unique',
          message: fromValidators.notUniqueValidatorMessage
        },
        {
          name: 'autocomplete',
          message: fromValidators.autocompleteValidatorMessage
        },
        {
          name: 'pin',
          message: fromValidators.pinValidatorMessage
        },
        {
          name: 'matDatepickerMin',
          message: fromValidators.minDatePickerValidatorMessage
        },
        {
          name: 'matDatepickerMax',
          message: fromValidators.maxDatePickerValidatorMessage
        },
        {
          name: 'pattern',
          message: fromValidators.patternValidatorMessage
        }
      ],
      wrappers: [
        { name: 'file-upload-wrapper', component: FileUploadWrapperComponent }
      ]
    })
  ],
  declarations: [
    FormlyFormFlexComponent,
    FormViewComponent,
    InfoFieldComponent,
    DateTimePickerComponent,
    AutocompleteTypeComponent,
    NgSelectTypeComponent,
    FileUploadComponent,
    FileUploadWrapperComponent,
    FileValueAccessorDirective,
    HtmlTypeComponent,
    TagInputComponent,
    RepeatTypeComponent,
    ButtonTypeComponent,
  ],
  exports: [
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    FormsModule,
    FormlyMaterialModule,
    FormlyFormFlexComponent,
    FormViewComponent
  ]
})
export class NgxLibFormlyModule { }
