import { FormControl, ValidationErrors } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';

import { DropdownItem } from '../models/dropdown-item.model';
import * as fromRegex from './regex.constants';

export const maxFileSize100MB = 104857600;


const MAX_PIN_LENGTH = 4;

export function integerValidator(control: FormControl): ValidationErrors {
  return fromRegex.onlyIntegersRegex.test(control.value) || !control.value
    ? null
    : { integer: true };
}

export function decimalValidator(control: FormControl): ValidationErrors {
  return fromRegex.hasDecimalsRegex.test(control.value) || !control.value
    ? null
    : { decimal: true };
}

export function specialCharactersValidator(
  control: FormControl
): ValidationErrors {
  return fromRegex.specialCharactersRegex.test(control.value) || !control.value
    ? { pattern: true }
    : null;
}

export function contactNumberValidator(control: FormControl): ValidationErrors {
  return fromRegex.contactNumberRegex.test(control.value) || !control.value
    ? null
    : { 'contact-number': true };
}

export function emailValidator(control: FormControl): ValidationErrors {
  return fromRegex.emailRegex.test(control.value) || !control.value
    ? null
    : { email: true };
}

export function pinValidator(ctrl: FormControl): ValidationErrors {
  let value: string = ctrl.value;
  if (value == undefined) value = '';
  const maxLength = MAX_PIN_LENGTH;
  const containsOnlyDigits = fromRegex.onlyIntegersRegex.test(value);
  const matchesLength = value.length === maxLength;
  return !value || (containsOnlyDigits && matchesLength) ? null : { pin: true };
}

export function fileUploadValidator({ value }: FormControl): ValidationErrors {
  if (!value) return null;
  const fileList: File[] = Array.from<File>(value);
  let fileExistsThatExceedsMax: boolean = fileList.some(
    file => file.size > maxFileSize100MB
  );
  return fileExistsThatExceedsMax ? { fileUpload: true } : null;
}

export function autocompleteValidator(
  options: DropdownItem[]
): ValidationErrors {
  return function({ value }: FormControl) {
    if (!value) {
      return null;
    }
    const derivedValue = typeof value === 'string' ? value : value.value;
    const optionExists = !!options.find(
      option => option.value === derivedValue
    );
    return optionExists ? null : { autocomplete: true };
  };
}

// Messages
export function integerValidatorMessage(error, field: FormlyFieldConfig) {
  return error && `"${field.formControl.value}" is not a valid integer`;
}

export function maxlengthValidationMessage(error, field: FormlyFieldConfig) {
  return (
    error &&
    `${field.templateOptions.label} cannot exceed ${field.templateOptions.maxLength} characters in length`
  );
}

export function decimalValidatorMessage(error, field: FormlyFieldConfig) {
  return error && `"${field.formControl.value}" is not a valid decimal`;
}

export function contactNumberValidatorMessage(error, field: FormlyFieldConfig) {
  return error && `"${field.formControl.value}" is not a valid contact number`;
}

export function emailValidatorMessage(error, field: FormlyFieldConfig) {
  return error && `"${field.formControl.value}" is not a valid email`;
}

export function notUniqueValidatorMessage(error, field: FormlyFieldConfig) {
  return error && `"${field.formControl.value}" is already in use`;
}

export function fileUploadValidatorMessage(error, field: FormlyFieldConfig) {
  return error && `"${field.formControl.value}" exceeds the maximum file size`;
}

export function autocompleteValidatorMessage(error, field: FormlyFieldConfig) {
  return (
    error &&
    `"${field.formControl.value}" is not a valid ${field.templateOptions.label}`
  );
}

export function pinValidatorMessage(error, field: FormlyFieldConfig) {
  const maxLength = MAX_PIN_LENGTH;
  return error && `${field.templateOptions.label} must be ${maxLength} digits`;
}

export function minDatePickerValidatorMessage(error, field: FormlyFieldConfig) {
  return (
    error && `${field.templateOptions.label} must be after the minimum date`
  );
}

export function maxDatePickerValidatorMessage(error, field: FormlyFieldConfig) {
  return (
    error && `${field.templateOptions.label} must be before the maximum date`
  );
}

export function patternValidatorMessage(error, field: FormlyFieldConfig) {
  return (
    error &&
    `${field.templateOptions.label} is restricted to alphanumeric, hyphen, space, and underscore characters`
  );
}
