import { BehaviorSubject, Observable } from 'rxjs';

export type ObservableQueryFunction<TData, TArgs> = (
  args: TArgs
) => Promise<TData>;

export interface ObservableDatasource<TData, TArgs> {
  queryFn: ObservableQueryFunction<TData, TArgs>;
}

/**
 * A convenience class that exposes the result of an asynchronous operation as an Observable.
 * Only the most recently initiated operation will cause the observable to emit.
 */
export class ObservableOperation<TData, TArgs> {
  private _version = 0;
  private _subject: BehaviorSubject<TData>;
  private _datasource: ObservableDatasource<TData, TArgs>;
  readonly data$: Observable<TData>;

  /**
   *
   * @param queryFn The query function to be performed whenever the refresh method is invoked.
   * @param initialState The initial state of the Observable.
   */
  constructor(
    queryFn:
      | ObservableQueryFunction<TData, TArgs>
      | ObservableDatasource<TData, TArgs>,
    initialState?: TData
  ) {
    this._datasource =
      typeof queryFn === 'function' ? { queryFn: queryFn } : queryFn;
    this._subject = new BehaviorSubject(initialState);
    this.data$ = this._subject.asObservable();
  }

  /**
   * Initiates a new query and raises the Observable event if the result is not stale (a result is stale if the originating query completes
   * after one or more subsequent queries have completed).
   * Returns an asynchronous result of true if the query completes without becoming stale, otherwise returns a result of false.
   * @param args The arguments to pass to the query function.
   */
  async refresh(args: TArgs): Promise<boolean> {
    let current = ++this._version;
    const result = await this._datasource.queryFn(args);
    let isFresh = this._version === current;

    if (isFresh) {
      this.onBeforeEmit(result);
      this._subject.next(result);
    }
    return Promise.resolve(isFresh);
  }

  get data(): TData {
    return this._subject.value;
  }

  /**
   * Invoked by the refresh method after a fresh query result has been received but before the Observable event is raised.
   * @param _data The query result data.
   */
  protected onBeforeEmit(_data: TData) {}
}
