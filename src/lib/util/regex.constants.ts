export const onlyIntegersRegex = /^[-+]?\d+$/;
export const hasDecimalsRegex = /^(\d+\.?\d{0,9}|\.\d{1,9})$/;
export const specialCharactersRegex = /[`~!@#$%^&*()+={}\/\\\[\]|:";'<>?,]/;
export const contactNumberRegex = /^[0-9\()+.\s]{8,20}$/;
export const emailRegex = /^\w+([\.-]?\w+)+@\w+([\.:]?\w+)+(\.[a-zA-Z0-9]{2,3})+$/;
