import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormlyAttributeEvent } from '@ngx-formly/core/lib/components/formly.field.config';
import { Observable } from 'rxjs';
import { DropdownItem } from '../models/dropdown-item.model';
import { ButtonFormField, CheckboxFormField, CustomFieldOptions, DateField, DropdownFormField, FieldChangeEventHandler, FieldValueExpression, FileUploadFormField, FormControl, FormControlType, FormElement, FormField, FormValidators, InfoElement, InputFormField, NumberFormField, RichTextFormField, TagInputFormField, TextAreaFormField, TextFormField, TextInputFormField } from './../formly-form/form-field';

export class FormFieldFactory {
  static createInfoField<TModel>(
    element: InfoElement<TModel>
  ): FormlyFieldConfig {
    let result = FormFieldFactory.createFormElement(element, <any>'infofield');
    result.templateOptions.custom = {
      text: element.text,
      type: element.type
    };
    return result;
  }

  static createFormElement<TModel>(
    el: FormElement<TModel>,
    type: FormControlType
  ): FormlyFieldConfig {
    let result = {
      // A non-existent value for Formly to "bind" to in the case of non-bound elements (otherwise an error occurs).
      key: '____nonbound',
      type: type,
      className: el.cssClass,
      hideExpression: el.hideExpression,
      templateOptions: {
        fxFlex: el.fxFlex,
        fxFlexSm: el.fxFlexSm
      }
    };

    applyDefaultCss(type, result, el);
    return result;
  }

  static createFormControl<TModel>(
    field: FormControl<TModel>,
    type: FormControlType
  ): FormlyFieldConfig {
    let result = FormFieldFactory.createFormElement(field, type);
    result.id = field.id;
    result.expressionProperties = {};
    let o = result.templateOptions;

    if (typeof field.disabled === 'boolean') {
      o.disabled = field.disabled;
    } else {
      addExpressionProp(result, 'disabled', field.disabled);
    }

    return result;
  }

  static createCustomComponentField<TModel, TValue>(
    field: FormField<TModel, TValue>,
    type: FormControlType
  ): FormlyFieldConfig {
    let result = FormFieldFactory.createFormControl(field, type);

    result.key = field.key;
    let o = result.templateOptions;
    o.label = field.fieldName;
    o.description = field.description;

    if (typeof field.required === 'boolean') {
      o.required = field.required;
    } else if (field.required == undefined) {
      // required must be explicitly set to false, otherwise some Formly validation may not work
      o.required = false;
    } else {
      addExpressionProp(result, 'required', field.required);
    }

    return result;
  }

  static createCheckbox<TModel>(
    field: CheckboxFormField<TModel>
  ): FormlyFieldConfig {
    let result = FormFieldFactory.createCustomComponentField(field, 'checkbox');
    result.templateOptions.indeterminate = field.indeterminate === true;
    result.templateOptions.change = createChangeEvent(field.change);
    return result;
  }

  static createBasicSelectField<TModel>(
    field: DropdownFormField<TModel>,
    src: DropdownItem[] | Observable<DropdownItem[]>,
    /**
     * If true, supresses the initial refresh of the `src` `DropdownInput`.
     */
    supressInitialRefresh: boolean = true
  ): FormlyFieldConfig {
    // Invoking DropdownProvider.resolve will cause the provider to peform an initial refresh
    let result = FormFieldFactory.createCustomComponentField(
      field,
      'ng-select'
    );
    decorateNgSelectField(
      field,
      result,
      src
    );
    return result;
  }


  static createTextField<TModel>(
    field: TextFormField<TModel>
  ): FormlyFieldConfig {
    let result = FormFieldFactory.createCustomComponentField(field, 'input');
    decorateTextField(field, result);
    return result;
  }

  static createTextAreaField<TModel>(
    field: TextAreaFormField<TModel>
  ): FormlyFieldConfig {
    let result = FormFieldFactory.createCustomComponentField(field, 'textarea');
    decorateTextField(field, result);
    result.templateOptions.rows = field.rows != undefined ? field.rows : 3;
    return result;
  }

  static createRichTextField<TModel>(
    field: RichTextFormField<TModel>
  ): FormlyFieldConfig {
    let result = FormFieldFactory.createCustomComponentField(
      field,
      'text-editor'
    );
    decorateRichTextField(field, result);
    return result;
  }

  static createTagInputField<TModel>(
    field: TagInputFormField<TModel>
  ): FormlyFieldConfig {
    return FormFieldFactory.createCustomComponentField(field, 'tag-input');
  }

  static createEmailField<TModel>(
    field: TextFormField<TModel>
  ): FormlyFieldConfig {
    let result = FormFieldFactory.createCustomComponentField(field, 'input');
    decorateEmailField(field, result);
    addValidators(result, 'email');
    return result;
  }

  static createIntegerField<TModel>(
    field: NumberFormField<TModel>
  ): FormlyFieldConfig {
    let result = FormFieldFactory.createCustomComponentField(field, 'input');
    decorateNumberField(field, result);
    addValidators(result, 'integer');
    return result;
  }

  static createDecimalField<TModel>(
    field: NumberFormField<TModel>
  ): FormlyFieldConfig {
    let result = FormFieldFactory.createCustomComponentField(field, 'input');
    decorateNumberField(field, result);
    addValidators(result, 'decimal');
    return result;
  }

  static createContactNumberField<TModel>(
    field: NumberFormField<TModel>
  ): FormlyFieldConfig {
    let result = FormFieldFactory.createCustomComponentField(field, 'input');
    decorateNumberField(field, result);
    addValidators(result, 'contact-number');
    return result;
  }

  static createDateField<TModel>(field: DateField<TModel>): FormlyFieldConfig {
    let result = FormFieldFactory.createCustomComponentField(
      field,
      'datepicker'
    );
    result.templateOptions.type = 'text';
    result.templateOptions.datepickerPopup = 'dd-MMMM-yyyy';
    if (result.className == undefined) {
      result.className = 'standard-datepicker';
    }

    addExpressionProp(result, 'datepickerOptions.min', field.minDate);
    addExpressionProp(result, 'datepickerOptions.max', field.maxDate);

    // result.templateOptions.dateChange = createChangeEvent(field.change);
    // result.templateOptions.change = createChangeEvent(field.change);

    return result;
  }

  static createDateTimeField<TModel>(
    field: DateField<TModel>
  ): FormlyFieldConfig {
    let result = FormFieldFactory.createCustomComponentField(field, <any>(
      'datetimepicker'
    ));
    decorateCustomField(field, result);
    return result;
  }

  static createFileUploadField<TModel>(
    field: FileUploadFormField<TModel>
  ): FormlyFieldConfig {
    let result = FormFieldFactory.createCustomComponentField(
      field,
      'file-upload'
    );
    decorateFileUploadField(field, result);
    addValidators(result, 'file-upload');
    return result;
  }

  static createButtonField<TModel, T>(
    field: ButtonFormField<TModel, T>
  ): FormlyFieldConfig {
    let result = FormFieldFactory.createFormControl(field, 'button');
    decorateButtonField(field, result);
    return result;
  }
}

//#region Decorator Functions

function decorateCustomField(
  field: FormField<any, any>,
  result: FormlyFieldConfig
): void {
  let o: CustomFieldOptions<any> = {
    change: field.change
  };

  result.templateOptions.custom = o;
}

function decorateNgSelectField(
  field: DropdownFormField<any>,
  result: FormlyFieldConfig,
  data: DropdownItem[] | Observable<DropdownItem[]>
): void {
  mergeExpressionProps(result, field);
  // addExpressionProp(result, 'disabled', field.disabled);

  result.templateOptions = {
    ...result.templateOptions,
    bindLabel: field.valueAsLabel ? 'value' : undefined,
    selectable: true,
    removable: field.removable == undefined || field.removable === true,
    addOnBlur: true,
    placeholder: field.fieldName,
    partialMatch: false,
    multiple: field.multiValue,
    clearable: field.clearable === undefined ? true : field.clearable === true,
    deselectMode: field.deselectMode,
    options: data,
    onAdd: field.add,
    onRemove: field.remove,
    onChange: field.change,
    fxFlex: field.fxFlex,
    fxFlexSm: field.fxFlexSm
  };
}


function decorateInputField<TModel>(
  field: InputFormField<TModel, any>,
  result: FormlyFieldConfig
) {
  result.modelOptions = {
    debounce: {
      default: field.debounceTime
    }
  };

  result.templateOptions.keypress = createDomEvent(field.keyPress);
  result.templateOptions.keydown = createDomEvent(field.keyDown);
  result.templateOptions.keyup = createDomEvent(field.keyUp);
  result.templateOptions.change = createChangeEvent(field.change);
}

function decorateNumberField<TModel>(
  field: NumberFormField<TModel>,
  result: FormlyFieldConfig
): void {
  decorateInputField(field, result);
  result.templateOptions.min = field.minValue;
  result.templateOptions.max = field.maxValue;
}

function decorateTextInputField<TModel>(
  field: TextInputFormField<TModel>,
  result: FormlyFieldConfig
) {
  decorateInputField(field, result);
  result.templateOptions.minLength = field.minLength;
  result.templateOptions.maxLength = field.maxLength;
}

function decorateTextField<TModel>(
  field: TextFormField<TModel>,
  result: FormlyFieldConfig
): void {
  decorateTextInputField(field, result);

  if (field.safeInput === true) {
    addValidators(result, 'safe-input');
  }
}

function decorateRichTextField<TModel>(
  field: TextFormField<TModel>,
  result: FormlyFieldConfig
): void {
  decorateTextInputField(field, result);
}

function decorateEmailField<TModel>(
  field: TextFormField<TModel>,
  result: FormlyFieldConfig
): void {
  decorateTextInputField(field, result);
}

function decorateFileUploadField<TModel>(
  field: FileUploadFormField<TModel>,
  result: FormlyFieldConfig
): void {
  decorateTextInputField(field, result);
  result.templateOptions.floatLabel = field.floatLabel;
  result.templateOptions.appearance = field.appearance;
}

function decorateButtonField<TModel, T>(
  field: ButtonFormField<TModel, T>,
  result: FormlyFieldConfig
): void {
  // decorateTextField(field, result);
  mergeExpressionProps(result, field);
  addExpressionProp(result, 'disabled', field.disabled);

  result.templateOptions = {
    text: field.text,
    color: field.color,
    type: field.type,
    click: field.click,
    fxFlex: field.fxFlex
  };
}

//#endregion

//#region Other Helper Functions

function applyDefaultCss(
  baseName: string,
  formly: FormlyFieldConfig,
  field: FormElement<any>
): void {
  let className = (field.inline === true ? 'inline-' : 'standard-') + baseName;
  formly.className =
    formly.className == undefined
      ? className
      : formly.className + ' ' + className;
}

function mergeExpressionProps(
  formly: FormlyFieldConfig,
  field: FormControl<any>
) {
  if (field.expressionProperties) {
    formly.expressionProperties = {
      ...formly.expressionProperties,
      ...field.expressionProperties
    };
  }
}

function addExpressionProp(
  field: FormlyFieldConfig,
  path: string,
  expression: FieldValueExpression<any, any>
) {
  if (expression != undefined) {
    if (typeof expression !== 'function') {
      let v = expression;
      expression = () => (v ? v : undefined);
    } else {
      // It seems that, under some conditions, Formly will assign a null (as opposed to "unassigned") value control's bound model property
      // after having evaluated an expression property IF the bound property is unassigned.
      // This can result in "expression changed after it was set" errors to occur if the expression assigns undefined.
      // To work around this, we wrap expression properties in proxy functions to ensure that undefined is always returned if the value is null.
      let ex = expression;
      expression = (model: any) => {
        let r = ex(model);
        return r ? r : undefined;
      };
    }
    field.expressionProperties['templateOptions.' + path] = expression;
  }
}

function addValidators(
  field: FormlyFieldConfig,
  ...validators: FormValidators[]
): void {
  if (!field.validators) {
    field.validators = { validation: [] };
  }

  let target: any[] = field.validators.validation;
  validators.forEach(v => {
    target.push(v);
  });
}

function createChangeEvent(
  handler: FieldChangeEventHandler<any>
): FormlyAttributeEvent {
  if (handler == undefined) return undefined;
  return (_field, arg) => {
    handler(arg);
  };
}

function createDomEvent(
  handler: (event: UIEvent, value: any) => void
): FormlyAttributeEvent {
  if (handler == undefined) return undefined;
  return (_field, value: UIEvent) => {
    handler(value, (<any>value.srcElement).value);
  };
}

//#endregion
