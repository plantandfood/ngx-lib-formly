import { FormlyFieldConfig } from '@ngx-formly/core';
import { ButtonFormField, CheckboxFormField, DateField, DropdownFormField, FieldValueExpression, FileUploadFormField, InfoElement, NumberFormField, RichTextFormField, TagInputFormField, TextAreaFormField, TextFormField } from './../formly-form/form-field';
import { FormFieldFactory } from './form-field-factory';
import { Observable } from 'rxjs';
import { DropdownItem } from '../models/dropdown-item.model';

export class FormFieldBuilder<TModel> {
  private _fields: FormlyFieldConfig[] = [];

  constructor() { }

  private append(f: FormlyFieldConfig): void {
    this._fields.push(f);
  }

  makeReadOnly(): void {
    this._fields.forEach(f => {
      f.templateOptions.disabled = true;
    });
  }

  checkbox(field: CheckboxFormField<TModel>): void {
    this.append(FormFieldFactory.createCheckbox(field));
  }

  infoField(field: InfoElement<TModel>): void {
    this.append(FormFieldFactory.createInfoField(field));
  }

  basicSelectField(field: DropdownFormField<TModel>, src: DropdownItem[] | Observable<DropdownItem[]>): void {
    this.append(FormFieldFactory.createBasicSelectField(field, src));
  }

  textField(field: TextFormField<TModel>): void {
    this.append(FormFieldFactory.createTextField(field));
  }

  textAreaField(field: TextAreaFormField<TModel>): void {
    this.append(FormFieldFactory.createTextAreaField(field));
  }

  richTextField(field: RichTextFormField<TModel>): void {
    this.append(FormFieldFactory.createRichTextField(field));
  }

  tagInputField(field: TagInputFormField<TModel>): void {
    this.append(FormFieldFactory.createTagInputField(field));
  }

  emailField(field: TextFormField<TModel>): void {
    this.append(FormFieldFactory.createEmailField(field));
  }

  integerField(field: NumberFormField<TModel>): void {
    this.append(FormFieldFactory.createIntegerField(field));
  }

  decimalField(field: NumberFormField<TModel>): void {
    this.append(FormFieldFactory.createDecimalField(field));
  }

  contactNumberField(field: NumberFormField<TModel>): void {
    this.append(FormFieldFactory.createContactNumberField(field));
  }

  dateField(field: DateField<TModel>): void {
    this.append(FormFieldFactory.createDateField(field));
  }

  startDateEndDate(
    options: {
      startDateKey?: string;
      startDateName?: string;
      endDateKey?: string;
      endDateName?: string;
      startDateRequiredExpression?: FieldValueExpression<TModel, boolean>;
      endDateRequiredExpression?: FieldValueExpression<TModel, boolean>;
    } = {}
  ): void {
    const startDateKey: string = options.startDateKey
      ? options.startDateKey
      : 'startDate';
    const endDateKey: string = options.endDateKey
      ? options.endDateKey
      : 'endDate';

    this.dateField({
      key: startDateKey,
      fieldName: options.startDateName
        ? options.startDateName
        : 'Start Date',
      inline: true,
      required: options.startDateRequiredExpression
        ? options.startDateRequiredExpression
        : true,
      maxDate: model => model[endDateKey]
    });

    this.dateField({
      key: endDateKey,
      fieldName: options.endDateName
        ? options.endDateName
        : 'End Date',
      inline: true,
      required: options.endDateRequiredExpression
        ? options.endDateRequiredExpression
        : false,
      hideExpression: model => {
        const result = model[startDateKey] == undefined;
        if (result) {
          // Avoid changing the model during the active change detection cycle
          setTimeout(() => {
            model[endDateKey] = undefined;
          });
        }
        return result;
      },
      minDate: model => model[startDateKey]
    });
  }

  dateTimeField(field: DateField<TModel>): void {
    this.append(FormFieldFactory.createDateTimeField(field));
  }

  fileUploadField(field: FileUploadFormField<TModel>): void {
    this.append(FormFieldFactory.createFileUploadField(field));
  }

  buttonField(field: ButtonFormField<TModel, any>): void {
    this.append(FormFieldFactory.createButtonField(field));
  }

  clear(): void {
    this._fields = [];
  }

  toArray(): FormlyFieldConfig[] {
    return Array.from(this._fields);
  }
}
