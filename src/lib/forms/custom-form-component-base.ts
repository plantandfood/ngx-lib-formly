import { FieldType } from '@ngx-formly/core';
import { CustomFieldOptions } from '../formly-form/form-field';

export class CustomFormComponentBase<_TModel, TValue> extends FieldType {
  private _custom: CustomFieldOptions<TValue>;

  ngOnInit(): void {
    super.ngOnInit();
    this._custom = this.to.custom;
  }

  protected custom(): CustomFieldOptions<TValue> {
    return this._custom;
  }

  protected onChange(value: TValue): void {
    if (this._custom.change != undefined) {
      this._custom.change(value);
    }
  }
}
