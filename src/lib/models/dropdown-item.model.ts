export interface DropdownItem {
  value?: string;
  label: string;
}

export class DropdownItem implements DropdownItem {
  constructor(public label: string, public value?: string) {}
}
