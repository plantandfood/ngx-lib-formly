import { FormlyTemplateOptions } from '@ngx-formly/core/lib/components/formly.field.config';
import { Subject } from 'rxjs';

export interface CustomTemplateOptions extends FormlyTemplateOptions {
  addTag?: boolean;
  multiple?: boolean;
  typeahead?: Subject<any>;
  loading?: Subject<any>;
  onAdd?: any;
  onRemove?: any;
  onClear?: any;
}
