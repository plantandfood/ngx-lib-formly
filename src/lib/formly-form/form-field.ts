import { FormlyFieldConfig } from '@ngx-formly/core';
import { Moment } from 'moment';
import { DropdownItem } from '../models/dropdown-item.model';

export type FormFieldConfig = FormlyFieldConfig[] | (() => FormlyFieldConfig[]);

export type FormValidators =
  | 'safe-input'
  | 'decimal'
  | 'integer'
  | 'contact-number'
  | 'email'
  | 'file-upload';

export type FormControlType =
  | 'input'
  | 'textarea'
  | 'ng-select'
  | 'checkbox'
  | 'datepicker'
  | 'tag-input'
  | 'text-editor'
  | 'file-upload'
  | 'button';

export type FieldChangeEventHandler<TValue> = (value: TValue) => void;
export type DomEventHandler<TEvent extends UIEvent, TValue> = (
  event: TEvent,
  value: TValue
) => void;

export type FieldValueExpression<TModel, TValue> =
  | TValue
  | ((model: TModel) => TValue);

export interface CustomFieldOptions<TValue> {
  change?: FieldChangeEventHandler<TValue | TValue[]>;
}

export interface FormElement<TModel> {
  hideExpression?:
    | string
    | boolean
    | ((model: TModel, formState: any) => boolean);
  cssClass?: string;
  description?: string;
  fxFlex?: string;
  fxFlexSm?: string;
  /**
   * If true, will (where supported) cause the form element to apply styling to render itself inline.
   */
  inline?: boolean;
}

export type InfoElementType = 'info' | 'warn' | 'error';

export interface InfoElement<TModel> extends FormElement<TModel> {
  text: string | (() => string);
  type?: InfoElementType;
}

export interface FormControl<TModel> extends FormElement<TModel> {
  id?: string;
  /** If true, will cause this `FormField` to be disabled by the user-interface. */
  disabled?: FieldValueExpression<TModel, boolean>;
  expressionProperties?: {
    [property: string]: string | ((model: any, formState: any) => boolean);
  };
}

export interface FormField<TModel, TValue> extends FormControl<TModel> {
  /** The model property that this `FormField` is bound to. */
  key: string;
  /** The text used to identify this `FormField` in the user interface. */
  fieldName: string;
  /** If true, will (where supported) allow the user to apply multiple values to this `FormField`. */
  multiValue?: boolean;
  /**  */
  change?: FieldChangeEventHandler<any | TValue | TValue[]>;
  /** Whether or not this `FormField` will be treated as a required field by the user-interface. */
  required?: FieldValueExpression<TModel, boolean>;
}

/**
 * Determines how the selected items in a list are automatically deselected.
 *
 * Options
 *
 * `never` (default) - selected items are never automatically deselected.
 *
 * `change` - selected items are deselected whenever the list items change or are repopulated.
 *
 *  `missing` - selected items are deselected whenever the list items change and they are not represented in the new set.
 */
export type ListDeselectMode = 'never' | 'change' | 'missing';

export interface DropdownFormField<TModel>
  extends FormField<TModel, DropdownItem> {
  /**
   * If true, causes the current value to be applied as the field label.
   */
  valueAsLabel?: boolean;
  /** The rule that determines how the selected items in the list are automatically deselected.  */
  deselectMode?: ListDeselectMode;
  /** Determines whether selected items in the list can be deselected from the user-interface. */
  removable?: boolean;
  clearable?: boolean;
  /** An event handler that is invoked when an item is added to the selected items collection from the user-interface. */
  add?: FieldChangeEventHandler<DropdownItem>;
  /** An event handler that is invoked when an item is removed from the selected items collection from the user-interface. */
  remove?: FieldChangeEventHandler<DropdownItem>;
}

export interface TextFilteredListField<TModel>
  extends DropdownFormField<TModel> {}

export interface CheckboxFormField<TModel> extends FormField<TModel, boolean> {
  change?: FieldChangeEventHandler<boolean>;
  indeterminate?: boolean;
}

export interface DateField<TModel> extends FormField<TModel, Date> {
  minDate?: FieldValueExpression<TModel, Date | string | Moment>;
  maxDate?: FieldValueExpression<TModel, Date | string | Moment>;
}

export interface InputFormField<TModel, T> extends FormField<TModel, T> {
  change?: FieldChangeEventHandler<T>;
  debounceTime?: number;
  keyPress?: DomEventHandler<KeyboardEvent, string>;
  keyDown?: DomEventHandler<KeyboardEvent, string>;
  keyUp?: DomEventHandler<KeyboardEvent, string>;
}

export interface NumberFormField<TModel>
  extends InputFormField<TModel, number> {
  minValue?: number;
  maxValue?: number;
}

export interface DecimalFormField<TModel> extends NumberFormField<TModel> {
  // maxPlaces?: number;
}

export interface TextInputFormField<TModel>
  extends InputFormField<TModel, string> {
  minLength?: number;
  maxLength?: number;
}

export interface TextFormField<TModel> extends TextInputFormField<TModel> {
  defaultValue?: string;
  safeInput?: boolean;
}

export interface TextAreaFormField<TModel> extends TextFormField<TModel> {
  rows?: number;
}

export interface TagInputFormField<TModel> extends FormField<TModel, string> {}

export interface RichTextFormField<TModel> extends TextInputFormField<TModel> {}

export interface FileUploadFormField<TModel> extends FormField<TModel, string> {
  floatLabel?: string;
  appearance?: string;
}

export interface ButtonFormField<TModel, T> extends FormControl<TModel> {
  click?: (value: T) => void;
  text?: string;
  color?: string;
  type?: string;
}
