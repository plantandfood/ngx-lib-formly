import { Component } from '@angular/core';
import {
  FormlyForm,
  FormlyFieldConfig,
  FormlyTemplateOptions
} from '@ngx-formly/core';

interface FormlyTemplateOptionsFlex extends FormlyTemplateOptions {
  fxFlex?: string;
}

interface FormlyFieldConfigFlex extends FormlyFieldConfig {
  templateOptions?: FormlyTemplateOptionsFlex;
}

@Component({
  selector: 'lib-formly-form-flex',
  template: `
    <div
      fxLayout="row wrap"
      fxLayoutAlign="left"
      fxLayoutGap="40px"
      class="content"
      fxLayout="row"
      fxLayout.sm="column"
      fxFlexFill
    >
      <formly-field
        *ngFor="let field of fields"
        [fxFlex]="field.templateOptions.fxFlex"
        [fxFlex.sm]="field.templateOptions.fxFlexSm"
        [form]="form"
        [field]="field"
        [ngClass]="field.className"
        [options]="options"
      >
      </formly-field>
    </div>

    <ng-content></ng-content>
  `
})
export class FormlyFormFlexComponent extends FormlyForm {
  fields: FormlyFieldConfigFlex[];
}
