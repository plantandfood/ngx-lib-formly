import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormFieldConfig } from '../form-field';

@Component({
  selector: 'lib-form-view',
  templateUrl: './form-view.component.html',
  styleUrls: ['./form-view.component.scss']
})
export class FormViewComponent<TModel> {

  private _model: TModel = <any>{};

  form = new FormGroup({});

  @Input() inline: boolean = false;
  @Input() fields: FormFieldConfig;

  constructor() {}

  @Input() get model(): TModel {
    return this._model;
  }

  set model(value: TModel) {
    if (value == undefined) {
      value = <any>{};
    }
    this._model = value;
  }

}
