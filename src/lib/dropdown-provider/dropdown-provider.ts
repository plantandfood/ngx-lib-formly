import { isObservable, Observable } from 'rxjs';
import { ObservableOperation } from '../util/observable-operation';
import { ExceptionMessageFormatter } from '../../lib/exception-message-formatter';
import { DropdownItem } from '../models/dropdown-item.model';
import { DropdownDatasource } from './dropdown-datasource';

export interface DropdownDatasourceResolveOptions {
  suppressRefresh?: boolean;
  // dynamic?: boolean;
}

export type DropdownQueryFunction = (filter: string) => Promise<DropdownItem[]>;

export type DropdownInput =
  | DropdownItem[]
  | Observable<DropdownItem[]>
  | DropdownProvider
  | DropdownDatasource;

export class DropdownProvider extends ObservableOperation<
  DropdownItem[],
  string
> {
  /**
   *
   * @param dataSource The query function to be performed whenever the refresh method is invoked.
   * @param initialState The initial state of the Observable.
   */
  constructor(dataSource: DropdownQueryFunction | DropdownDatasource) {
    super(
      typeof dataSource === 'function' ? dataSource : dataSource.queryFn,
      []
    );
  }

  /**
   * Creates a `DropdownProvider` instance from a `DropdownInput` (or uses the passed-in instance if `src` is already a `DropdownProvider`), and (unless explictly suppressed) invokes its `refresh` method.
   * @param src
   */
  static resolve(
    src: DropdownInput,
    options?: DropdownDatasourceResolveOptions
  ): DropdownProvider {
    if (Array.isArray(src)) {
      src = new DropdownProvider(DropdownDatasource.createStatic(src));
    } else if (isObservable(src)) {
      // src = createFromOptions(src, options);
      src = new DropdownProvider(DropdownDatasource.createStatic(src));
    } else if (!(src instanceof DropdownProvider)) {
      if (
        src instanceof DropdownDatasource ||
        typeof (<any>src).queryFn === 'function'
      ) {
        src = new DropdownProvider(<any>src);
      } else {
        throw new Error(ExceptionMessageFormatter.argument('src'));
      }
    }

    if (!(options != undefined && options.suppressRefresh)) {
      src.refresh('');
    }
    return src;
  }
}

// function createFromOptions(
//   src: Observable<DropdownItem[]>,
//   options: DropdownDatasourceResolveOptions
// ): DropdownProvider {
//   let ds =
//     options != undefined && options.dynamic === true
//       ? DropdownDatasource.createDynamic(src)
//       : DropdownDatasource.createStatic(src);
//   return new DropdownProvider(ds);
// }
