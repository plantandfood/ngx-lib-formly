import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ObservableDatasource } from '../util/observable-operation';
import { ExceptionMessageFormatter } from '../../lib/exception-message-formatter';
import { TypeHelper } from '../util/type-helper';
import { DropdownItem } from '../models/dropdown-item.model';
import { Disposables } from '../util/disposables';
import { DropdownQueryFunction } from './dropdown-provider';
import { DropdownProviderOptions } from './dropdown-provider-options';

/**
 * Describes the pre-defined `DropdownAutomap` functions.
 *
 * Options:
 *
 * `name` Maps to a `DropdownItem` in the format `{ value: src.name, label: src.name }`
 *
 * `uuid` Maps to a `DropdownItem` in the format `{ value: src.uuid, label: src.name }`
 *
 * `link` Maps to a `DropdownItem` in the format `{ value: src._links.self.href, label: src.name }`
 *
 * `string` Maps an input string, src, to a `DropdownItem` in the format `{ value: src, label: src }`
 *
 * `name|*` Maps an input string starting with `name|` to a `DropdownItem` in the format `{ value: name, label: name }`
 *
 * `name|uuid` Maps an input string in the format `name|uuid` to a `DropdownItem` in the format `{ value: uuid, label: name }`
 */
export type DropdownAutomap =
  | 'name'
  | 'uuid'
  | 'link'
  | 'string'
  | 'name|*'
  | 'name|uuid';

/**
 * A function for mapping an entity to a `DropdownItem`.
 */
export type DropdownMapFunction<T> = (item: T) => DropdownItem;

/**
 * A function invoked by `DropdownDatasource` instances to query a remote dataset.
 * @param filter The filter that should be applied to the remote dataset.
 */
export type DropdownDataFunction<T> = (
  filter: string
) => Observable<T[] |any[]>;

/**
 * Determines how the `DropdownDatasource.keymap` method should handle properties with empty (string) or undefined property values.
 *
 * `skip` - the property will not be added to the result
 * `key` - the property name will be asigned to the `DropdownItem` label.
 */
export type KeymapMode = 'skip' | 'key';

/**
 * Represents a datasource for a `DropdownProvider`.
 */
// @dynamic
export abstract class DropdownDatasource
  implements ObservableDatasource<DropdownItem[], string> {
  abstract readonly queryFn: DropdownQueryFunction;

  /**
   * Maps an object to a `DropdownItem` array by treating it as a key/value collection.
   */
  static keymap<T>(src: T, mode: KeymapMode = 'key'): DropdownItem[] {
    if (src == undefined)
      throw new Error(ExceptionMessageFormatter.argumentNull('src'));

    return keymapFunction(src, mode);
  }

  /**
   * Asynchronously maps the subject of an `Observable` to a `DropdownItem` array by treating it as a key/value collection.
   */
  static keymapAsync<T>(
    src: Observable<T>,
    mode: KeymapMode = 'key'
  ): Observable<DropdownItem[]> {
    if (src == undefined)
      throw new Error(ExceptionMessageFormatter.argumentNull('src'));

    return src.pipe(map(input => keymapFunction(input, mode)));
  }

  /**
   * Converts an input array to a `DropdownItem` array using one of the pre-defined `DropdownAutomap` functions.
   */
  static automap<T>(src: T[], automap: DropdownAutomap): DropdownItem[] {
    if (src == undefined)
      throw new Error(ExceptionMessageFormatter.argumentNull('src'));
    if (automap == undefined)
      throw new Error(ExceptionMessageFormatter.argumentNull('automap'));
    return src.map(resolveMapFn(automap));
  }

  /**
   * Asynchronously maps an array which is the subject of an `Observable` to a  `DropdownItem` array using one of the pre-defined `DropdownAutomap` functions.
   */
  static automapAsync<T>(
    src: Observable<T[]>,
    automap: DropdownAutomap
  ): Observable<DropdownItem[]> {
    if (src == undefined)
      throw new Error(ExceptionMessageFormatter.argumentNull('src'));
    if (automap == undefined)
      throw new Error(ExceptionMessageFormatter.argumentNull('automap'));

    return src.pipe(map(input => input.map(resolveMapFn(automap))));
  }

  /**
   * Converts an input item to a `DropdownItem` item using one of the pre-defined `DropdownAutomap` functions.
   */
  static automapItem<T>(src: T, automap: DropdownAutomap): DropdownItem {
    if (src == undefined)
      throw new Error(ExceptionMessageFormatter.argumentNull('src'));
    if (automap == undefined)
      throw new Error(ExceptionMessageFormatter.argumentNull('automap'));
    return resolveMapFn(automap)(src);
  }

  /**
   * Resolves the `DropdownMapFunction` for the specified `DropdownAutomap`.
   */
  static getAutomapFunction<T>(
    automap: DropdownAutomap
  ): DropdownMapFunction<T> {
    return resolveMapFn(automap);
  }

  /**
   * Creates a `DropdownDatasource` that maintains a fixed internal `DropdownItem` collection.
   * Any filtering is performed on the static internal collection.
   */
  static createStatic(
    src: DropdownItem[] | Observable<DropdownItem[]>,
    options?: DropdownProviderOptions
  ): DropdownDatasource {
    if (src == undefined)
      throw new Error(ExceptionMessageFormatter.argumentNull('src'));
    return new SimpleDropdownDatasource(staticQueryFn(src, options));
  }

  /**
   * Creates a `DropdownDatasource` that maintains a fixed internal `DropdownItem` collection which is
   * derived by converting an input entity array to an array of `DropDownItem` using the specified map function.
   * Any filtering is performed on the static internal collection.
   */
  static createMapStatic<T>(
    src: any[] | Observable<any[] | any[]>,
    mapFn: DropdownMapFunction<T> | DropdownAutomap,
    options?: DropdownProviderOptions
  ): DropdownDatasource {
    if (src == undefined)
      throw new Error(ExceptionMessageFormatter.argumentNull('src'));
    let mFn = resolveMapFn(mapFn);
    if (Array.isArray(src))
      return new SimpleDropdownDatasource(staticQueryFn(src.map(mFn), options));
    return new SimpleDropdownDatasource(
      staticQueryFn(reduceSrc(src, mFn), options)
    );
  }

  // static createDynamic(
  //   src: DropdownDataFunction<DropdownItem> | Observable<DropdownItem[]>
  // ): DropdownDatasource {
  //   if (typeof src !== 'function') {
  //     let obs = src;
  //     src = (_filter: string) => obs;
  //   }

  //   return DropdownDatasource.createMapDynamic(src, item => item);
  // }

  /**
   * Creates a `DropdownDatasource` that utilises a query function to manage a dynamic `DropdownItem` collection which is
   * derived by converting the query result to an array of `DropDownItem` using the specified map function.
   * Filtering is performed on the remote dataset by the query function.
   */
  static createMapDynamic<T>(
    query: DropdownDataFunction<T>,
    mapFn: DropdownMapFunction<T> | DropdownAutomap
  ): DropdownDatasource {
    if (query == undefined)
      throw new Error(ExceptionMessageFormatter.argumentNull('query'));
    let mFn = resolveMapFn(mapFn);
    let fn: DropdownQueryFunction = (filter: string) => {
      return new Promise<DropdownItem[]>((resolve, reject) => {
        Disposables.global.subscribeReplayOnce(
          reduceSrc(query(filter), mFn),
          items => {
            resolve(items);
          },
          error => {
            reject(error);
          }
        );
      });
    };

    return new SimpleDropdownDatasource(fn);
  }
}

class SimpleDropdownDatasource extends DropdownDatasource {
  constructor(readonly queryFn: DropdownQueryFunction) {
    super();
    if (queryFn == undefined)
      throw new Error(ExceptionMessageFormatter.argumentNull('queryFn'));
  }
}

function reduceSrc(
  src: Observable<any[] | any[]>,
  mapFn: DropdownMapFunction<any>
): Observable<DropdownItem[]> {
  return src.pipe(
    map((result: any[] | any[]) => {
      if (Array.isArray(result)) return result.map(mapFn);
      throw result;
    })
  );
}

function staticQueryFn(
  src: DropdownItem[] | Observable<DropdownItem[]>,
  options: DropdownProviderOptions
): DropdownQueryFunction {
  if (src == undefined)
    throw new Error(ExceptionMessageFormatter.argumentNull('src'));

  let fn: DropdownQueryFunction = Array.isArray(src)
    ? (filter: string = undefined) => {
        return filterFunction(src, filter);
      }
    : async (filter: string) => {
        let promise: Promise<DropdownItem[]> = new Promise(
          (resolve, reject) => {
            try {
              Disposables.global.subscribeReplayOnce(src, items => {
                applyOptions(items, options);
                resolve(items);
              });
            } catch (e) {
              reject(e);
            }
          }
        );
        const items = await promise;
        return await filterFunction(items, filter);
      };

  return fn;
}

function filterFunction(
  src: DropdownItem[],
  filter: string = undefined
): Promise<DropdownItem[]> {
  return Promise.resolve(src.filter(item => item.label.indexOf(filter) >= 0));
}

function resolveMapFn<T>(
  mapFn: DropdownMapFunction<T> | DropdownAutomap
): DropdownMapFunction<T> {
  if (typeof mapFn === 'function') return mapFn;
  if (typeof mapFn !== 'string')
    throw new Error(ExceptionMessageFormatter.argument('mapFn'));

  switch (mapFn) {
    case 'uuid':
      return (item: any) => ({ value: item.uuid, label: item.name });

    case 'name':
      return (item: any) => ({ value: item.name, label: item.name });

    case 'string':
      return (item: any) => {
        if (item == undefined) {
          item = '';
        } else if (typeof item === 'object' && item.toString) {
          item = item.toString();
        }
        return { value: item, label: item };
      };

    case 'name|*':
      return (item: any) => {
        if (item == undefined) {
          item = '';
        } else {
          let idx = item.indexOf('|');
          if (idx >= 0) {
            item = item.substr(0, idx);
          }
        }
        return { value: item, label: item };
      };

    case 'name|uuid':
      return (item: any) => {
        let value = '';
        let label = '';
        if (item != undefined) {
          let idx = item.indexOf('|');
          if (idx >= 0) {
            label = item.substr(0, idx);
            value = item.substr(idx + 1, item.length);
          }
        }
        return { value: value, label: label };
      };

    case 'link':
      return (item: any) => ({
        value: item._links.self.href,
        label: item.name
      });

    default:
      throw new Error(ExceptionMessageFormatter.argument('mapFn'));
  }
}

function applyOptions(
  items: DropdownItem[],
  options: DropdownProviderOptions
): void {
  if (options != undefined && options.addEmpty === true) {
    items.unshift({
      label: options.emptyLabel ? options.emptyLabel : 'None',
      value: options.emptyValue
    });
  }
}

function keymapFunction<T>(src: T, mode: KeymapMode) {
  let result: DropdownItem[] = [];
  Object.keys(src).forEach(key => {
    let value = src[key];
    let hasValue = !TypeHelper.isStringNullOrEmpty(value);
    if (!hasValue && mode === 'skip') return;
    result.push({
      value: key,
      label: hasValue ? value : key
    });
  });

  return result;
}
