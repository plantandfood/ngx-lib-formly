import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/core';

@Component({
  selector: 'app-html-type',
  template: `
    <div class="html-type" [innerHtml]="value"></div>
 `,
  styles: [
    `
      .html-type {
        padding-bottom: 1.25em;
        white-space: pre-line;
      }
    `
  ]
})
export class HtmlTypeComponent extends FieldType {
  get value(): string {
    return this.formControl.value || '';
  }
}
