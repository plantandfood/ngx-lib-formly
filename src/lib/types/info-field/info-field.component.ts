import { Component, OnInit } from '@angular/core';
import { FormElement } from '../../forms/form-element';
import { InfoElementType } from '../../formly-form/form-field';

export interface InfoFieldOptions {
  text: string | (() => string);
  type: InfoElementType;
}

@Component({
  selector: 'lib-info-field',
  templateUrl: './info-field.component.html',
  styleUrls: ['./info-field.component.scss']
})
export class InfoFieldComponent extends FormElement<InfoFieldOptions>
  implements OnInit {
  private _textFn: () => string;
  private _type: InfoElementType = 'info';

  get type(): InfoElementType {
    return this._type;
  }

  get text(): string {
    return this._textFn ? this._textFn() : '';
  }

  ngOnInit() {
    super.ngOnInit();
    if (this.custom == undefined) return;
    switch (typeof this.custom.text) {
      case 'function':
        this._textFn = this.custom.text;
        break;

      case 'string':
        this._textFn = () => <string>this.custom.text;
        break;
    }

    if (this.custom.type != undefined) {
      this._type = this.custom.type;
    }
  }
}
