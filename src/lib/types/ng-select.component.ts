import { Component, OnInit, ViewChild } from '@angular/core';
import { NgOption, NgSelectComponent } from '@ng-select/ng-select';
import { FieldType } from '@ngx-formly/core';
import { Observable, Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { CustomTemplateOptions } from '../models/custom-template-options.model';
import { ListDeselectMode } from '../formly-form/form-field';
import { Disposables } from '../util/disposables';
import { DropdownItem } from '../models/dropdown-item.model';

// Repository and Documentation: https://github.com/ng-select/ng-select

@Component({
  selector: 'app-ng-select-type',
  template: `
    <ng-select
      [ngClass]="{
        'ng-select-required': to.required
      }"
      [items]="items"
      [bindLabel]="labelProp"
      [bindValue]="valueProp"
      [multiple]="to.multiple"
      [addTag]="to.addTag"
      [loading]="to.loading"
      [clearable]="to.clearable"
      (clear)="onClear()"
      [typeahead]="to.typeahead"
      [placeholder]="to.placeholder"
      [formControl]="formControl"
      (search)="onSearch($event)"
      (add)="onAdd($event)"
      (remove)="onRemove($event)"
      (change)="onChange($event)"
    >
    </ng-select>
  `
})
export class NgSelectTypeComponent extends FieldType implements OnInit {
  private _disposables: Disposables = new Disposables();
  private _search: Subject<string>;
  to: CustomTemplateOptions;
  @ViewChild(NgSelectComponent)
  control: NgSelectComponent;

  get labelProp(): string {
    return this.to.bindLabel || 'label';
  }

  get valueProp(): string {
    return this.to.bindValue || 'value';
  }

  get items(): any[] | Observable<DropdownItem[]> {
    let items: any[] |  Observable<DropdownItem[]>;
     if(this.to.options === undefined) return [];
     if(Array.isArray(this.to.options)) {
      return this.to.options;
     } else if (this.to.options instanceof Observable) {
      this._disposables.addSubscription(
       this.to.options.subscribe(options => {
         items = options;
       })
      );
     }
    return items;
  }

  ngOnInit(): void {
    this.subscribeToValueChange();
    if (this.to.onSearch) {
      let interval = this.to.debounceTime ? this.to.debounceTime : 0;
      this._search = new Subject();

      this._disposables.addSubscription(
        this._search.pipe(debounceTime(interval)).subscribe(filter => {
          this.to.onSearch(filter);
        })
      );
    }

    if (<ListDeselectMode>this.to.deselectMode === 'change') {
      this._disposables.subscribeTo(
        <Observable<any>>this.to.options,
        (_options: any[]) => {
          this.control.selectedItems.forEach(item => {
            this.control.unselect(item);
          });
        }
      );
    } else if (
      <ListDeselectMode>this.to.deselectMode === 'missing' &&
      this.to.options &&
      !Array.isArray(this.to.options)
    ) {
      this._disposables.subscribeTo(
        <Observable<any>>this.to.options,
        (options: any[]) => {
          let toRemove: NgOption[] = [];
          this.control.selectedItems.forEach(item => {
            if (
              options.findIndex(
                cmp => cmp[this.valueProp] === item.value[this.valueProp]
              ) < 0
            ) {
              toRemove.push(item);
            }
          });

          toRemove.forEach(target => {
            this.control.unselect(target);
          });
        }
      );
    }
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this._disposables.dispose();
  }

  onSearch(filter: any) {
    if (this._search) {
      this._search.next(filter != undefined ? filter.term : '');
    }
  }

  onClear() {
    if (this.to.onClear) this.to.onClear();
  }

  onAdd(item: any) {
    if (this.to.onAdd) this.to.onAdd(item);
  }

  onRemove(item: any) {
    if (this.to.onRemove) this.to.onRemove(item.value);
  }

  onChange(item: any) {
    if (this.to.onChange) this.to.onChange(item);
  }

  private subscribeToValueChange() {
    this._disposables.addSubscription(
      this.formControl.valueChanges.subscribe((value: string) =>
        this.to.ngSelectChange ? this.to.ngSelectChange(value) : null
      )
    );
  }
}
