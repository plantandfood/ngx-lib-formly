import { FormlyTemplateOptions } from '@ngx-formly/core';
import { Component, ViewChild, OnInit } from '@angular/core';
import { FieldType } from '@ngx-formly/material';
import { MatInput } from '@angular/material/input';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { MatOption } from '@angular/material';
import { DropdownItem } from '../models/dropdown-item.model';

@Component({
  selector: 'formly-autocomplete-type',
  template: `
    <input matInput
      [matAutocomplete]="auto"
      [formControl]="formControl"
      [formlyAttributes]="field"
      [placeholder]="to.placeholder">
    <mat-autocomplete #auto="matAutocomplete" [displayWith]="displayFn">
      <mat-option *ngFor="let option of filteredOptions | async" [value]="option.value">
        {{ option.label }}
      </mat-option>
    </mat-autocomplete>
  `
})
export class AutocompleteTypeComponent extends FieldType implements OnInit {
  @ViewChild(MatInput) formFieldControl: MatInput;

  filteredOptions: Observable<DropdownItem[]>;

  ngOnInit() {
    super.ngOnInit();
    this._setFilteredOptions(this.to);
  }

  displayFn(value?: string): string | undefined {
    const options = this.options as MatOption[];
    return value ? options.find(o => o.value === value).viewValue : undefined;
  }

  private _setFilteredOptions(templateOptions: FormlyTemplateOptions) {
    const { filter, options } = templateOptions;
    this.filteredOptions = this.formControl.valueChanges.pipe(
      startWith<string | DropdownItem>(''),
      map(value => (typeof value === 'string' ? value : value.label)),
      map(
        (label: string) =>
          filter
            ? filter(label)
            : this._defaultFilter(label, options as DropdownItem[])
      )
    );
  }

  private _defaultFilter(label: string, options: DropdownItem[]) {
    return options.filter(
      option => option.label.toLowerCase().indexOf(label.toLowerCase()) === 0
    );
  }
}
