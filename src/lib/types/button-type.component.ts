import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/core';

@Component({
  selector: 'formly-field-button',
  template: `
    <div>
      <button
        mat-flat-button
        [type]="to.type"
        [color]="to.color"
        [disabled]="to.disabled"
        (click)="onClick($event)"
      >
        {{ to.text }}
      </button>
    </div>
  `
})
export class ButtonTypeComponent extends FieldType {
  onClick($event) {
    if (this.to.click) {
      this.to.click($event);
    }
  }
}
