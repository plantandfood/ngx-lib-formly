import { Component, ViewChild } from '@angular/core';
import { MatDatepickerInput } from '@angular/material';
import { Moment } from 'moment';
import { CustomFormComponentBase } from '../../forms/custom-form-component-base';
import { DropdownItem } from '../../models/dropdown-item.model';
import { FormatHelper } from '../../format-helper';

@Component({
  selector: 'lib-datetimepicker-field',
  templateUrl: './datetime-picker.component.html',
  styleUrls: ['./datetime-picker.component.scss']
})
export class DateTimePickerComponent<TModel> extends CustomFormComponentBase<
  TModel,
  Date
> {
  private _value: Date;

  @ViewChild(MatDatepickerInput) picker: MatDatepickerInput<Moment>;

  hour: string = '0';
  minute: string = '0';

  hourOptions: DropdownItem[] = [];
  minuteOptions: DropdownItem[] = [];

  constructor() {
    super();

    for (let i = 0; i < 24; i++) {
      this.hourOptions.push({
        value: i.toString(),
        label: FormatHelper.formatTimeUnit(i)
      });
    }

    for (let i = 0; i < 60; i++) {
      this.minuteOptions.push({
        value: i.toString(),
        label: FormatHelper.formatTimeUnit(i)
      });
    }
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.updateControls();
  }

  pickerChange() {
    this.updateModel();
    this.onChange(this._value);
  }

  hourChange(args: { srcElement: { value: string } }): void {
    this.hour = args.srcElement.value;
    this.updateModel();
    this.onChange(this._value);
  }

  minuteChange(args: { srcElement: { value: string } }): void {
    this.minute = args.srcElement.value;
    this.updateModel();
    this.onChange(this._value);
  }

  private updateModel(): void {
    let d: Date = this.picker.value ? this.picker.value.toDate() : undefined;
    if (d) {
      d.setMilliseconds(0);
      d.setHours(parseInt(this.hour));
      d.setMinutes(parseInt(this.minute));
    }

    this._value = d;
    this.formControl.setValue(d);
    this.formControl.markAsDirty();
    this.formControl.markAsTouched();
  }

  private updateControls(): void {
    let d: Date;
    let v = this.form.value[this.field.key];

    if (typeof v === 'object') {
      d = v;
    } else if (typeof v === 'string' && v !== '') {
      d = new Date(v);
    } else {
      d = undefined;
    }

    if (d != undefined) {
      this.hour = d.getHours().toString();
      this.minute = d.getMinutes().toString();
      this._value = d;
    }

    this.picker.value = <any>d;
  }
}
