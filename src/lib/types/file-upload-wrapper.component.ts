import { Component, ViewChild, ViewContainerRef } from '@angular/core';
import { FieldWrapper } from '@ngx-formly/core';

@Component({
  selector: 'app-file-upload-wrapper',
  template: `
    <div>
      <ng-container #fieldComponent></ng-container>
      <mat-error [id]="null">
        <formly-validation-message [field]="field"></formly-validation-message>
      </mat-error>
      <mat-hint *ngIf="to.description" [id]="null">{{ to.description }}</mat-hint>
    </div>
  `
})
export class FileUploadWrapperComponent extends FieldWrapper {
  @ViewChild('fieldComponent', { read: ViewContainerRef })
  fieldComponent: ViewContainerRef;
}
