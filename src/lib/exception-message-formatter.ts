
// @dynamic
export class ExceptionMessageFormatter {
  static argument(parameterName: string, detail?: string): string {
    return appendDetail(`The argument "${parameterName}" is invalid.`, detail);
  }

  static argumentNull(parameterName: string): string {
    return `The argument "${parameterName}" cannot be null.`;
  }

  static argumentOutOfRange(
    parameterName: string,
    lower: number | string,
    upper: number | string
  ): string {
    return `The argument "${parameterName}" must be between ${lower} and ${upper}.`;
  }

  static argumentBelowBound(
    parameterName: string,
    lowerBound: number | string
  ): string {
    return `The argument "${parameterName}" must be greater than or equal to ${lowerBound}.`;
  }

  static argumentAboveBound(
    parameterName: string,
    upperBound: number | string
  ): string {
    return `The argument "${parameterName}" must be less than or equal to ${upperBound}.`;
  }

  static argumentMustBeInt(parameterName: string) {
    return `The argument "${parameterName}" must be an integer.`;
  }

  static notImplemented(): string {
    return 'Not implemented.';
  }

  static notSupported(operationName: string) {
    return `${operationName} is not supported.`;
  }


  static resolverFailed(typeName: string): string {
    return `Failed to resolve ${typeName} on the active route.`;
  }

  static propertyNotAssigned(
    propertyName: string,
    type?: string | any
  ): string {
    let result = `The property "${propertyName}" must be assigned`;
    if (type != undefined) {
      if (typeof type !== 'string' && type.constructor != undefined) {
        type = type.constructor.name;
      }
      result = `[${type}] ${result}`;
    }
    return result;
  }
}

function appendDetail(message: string, detail: string): string {
  if (detail) {
    message += ' ' + detail;
  }
  return message;
}
